package de.hicedevelopments.secretsantaapp.di

import de.hicedevelopments.secretsantaapp.presentation.login.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { LoginViewModel() }
}
