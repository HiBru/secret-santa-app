package de.hicedevelopments.secretsantaapp

import android.app.Application
import de.hicedevelopments.secretsantaapp.di.databaseModule
import de.hicedevelopments.secretsantaapp.di.repoModule
import de.hicedevelopments.secretsantaapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class SecretSantaApp : Application() {

    private val appModules = listOf(
        databaseModule,
        repoModule,
        viewModelModule
    )

    override fun onCreate() {
        super.onCreate()
        startTimber()
        startKoin()
    }

    private fun startKoin() {
        startKoin {
            androidLogger(
                if (BuildConfig.DEBUG) Level.ERROR else Level.NONE
            )
            androidContext(this@SecretSantaApp)
            modules(appModules)
        }
    }

    private fun startTimber() {
        Timber.plant(Timber.DebugTree())
    }
}
