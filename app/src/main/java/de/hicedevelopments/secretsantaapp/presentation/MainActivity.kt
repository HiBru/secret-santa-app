package de.hicedevelopments.secretsantaapp.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import com.ramcosta.composedestinations.DestinationsNavHost
import de.hicedevelopments.secretsantaapp.presentation.login.NavGraphs
import de.hicedevelopments.secretsantaapp.presentation.ui.theme.SecretSantaAppTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SecretSantaAppTheme {
                Surface(color = MaterialTheme.colors.background) {
                    DestinationsNavHost(navGraph = NavGraphs.root)
                }
            }
        }
    }
}
